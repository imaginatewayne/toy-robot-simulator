package me.wayne.allan.cli.clisessionrunner;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import me.wayne.allan.cli.CliSession;
import me.wayne.allan.cli.Console;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileCliSessionRunner implements CliSessionRunner {

    private Console console;
    private String filePath;
    private CliSession cliSession;

    @Inject
    public FileCliSessionRunner(Console console, CliSession cliSession, @Named("filePath")String filePath) {
        this.console = console;
        this.cliSession = cliSession;
        this.filePath = filePath;
    }

    @Override
    public void run() {
        try {
            for(String input: Files.readAllLines(Paths.get(this.filePath))){
                try {
                    cliSession.runCommand(input);
                } catch (Exception e) {
                    console.error(e.getMessage());
                }
            }
        } catch (IOException e) {
            console.error(e.getMessage());
        }
    }

}
