package me.wayne.allan.cli;

import me.wayne.allan.toyrobotlogic.FacingFactory;
import me.wayne.allan.toyrobotlogic.FacingParseException;
import me.wayne.allan.toyrobotlogic.NavigationUtils;
import me.wayne.allan.toyrobotlogic.command.*;

import java.util.ArrayList;

public class ToyRobotCommandFactory {
    public static ToyRobotCommand getCommand(CliCommand cliCommand, ArrayList<String> args) throws FacingParseException {
        switch (cliCommand) {
            case PLACE: return new PlaceToyRobotCommand(
                                Integer.parseInt(args.get(0)),
                                Integer.parseInt(args.get(1)),
                                FacingFactory.getFacing(args.get(2)));
            case MOVE: return new MoveToyRobotCommand();
            case LEFT: return new TurnToyRobotCommand(NavigationUtils.Turn.LEFT);
            case RIGHT: return new TurnToyRobotCommand(NavigationUtils.Turn.RIGHT);
            default: return new ReportToyRobotCommand();
        }
    }
}
