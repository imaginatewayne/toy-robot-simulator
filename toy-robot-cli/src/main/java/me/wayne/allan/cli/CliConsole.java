package me.wayne.allan.cli;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * Created by wayne on 12/07/2014.
 */
public class CliConsole implements Console {

    public static final int SUCCESS = 0;
    public static final int FAILED = 1;

    boolean quietMode = false;

    @Inject
    public CliConsole(@Named("quiteMode")boolean quietMode) {
        this.quietMode = quietMode;
    }

    @Override
    public void write(String message, Object... args) {
        if(!quietMode){
            System.out.println(String.format(message, args));
        }
    }

    @Override
    public void error(String message, Object... args) {
        if(!quietMode){
            System.err.println("Error: " + String.format(message, args));
        }
    }

    @Override
    public void exit(int exitCode, String message, Object... args) {
        if(!quietMode){
            message = String.format(message,args);
            if(exitCode == SUCCESS){
                write(message);
            }else{
                error(message);
            }
        }
        System.exit(exitCode);
    }

    @Override
    public void exit(String message, Object... args) {
        exit(SUCCESS,message,args);
    }
}
