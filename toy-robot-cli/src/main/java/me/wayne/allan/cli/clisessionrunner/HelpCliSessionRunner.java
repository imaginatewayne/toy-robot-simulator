package me.wayne.allan.cli.clisessionrunner;

import me.wayne.allan.cli.CliOptions;
import org.apache.commons.cli.HelpFormatter;

public class HelpCliSessionRunner implements CliSessionRunner {

    @Override
    public void run() {
        new HelpFormatter().printHelp("trs", CliOptions.getCliOptions(), true);
    }
}
