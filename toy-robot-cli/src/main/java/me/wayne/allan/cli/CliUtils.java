package me.wayne.allan.cli;

import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidArgumentException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseUnknownCommandException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidNumberOfArgsException;

import java.util.*;

/**
 * Created by wayne on 12/07/2014.
 */
public class CliUtils {

    public static final int COMMAND_POSITION = 0;

    public static ArrayList<String> parseArgs(CliCommand cliCommand, List<String> cmdComponents) throws ToyRobotCommandParseInvalidNumberOfArgsException, ToyRobotCommandParseInvalidArgumentException {
        if(cmdComponents.size()<=1){
            return null;
        }
        String command = cmdComponents.remove(COMMAND_POSITION);
        if(cliCommand.regExArgValidations == null){
            throw new ToyRobotCommandParseInvalidNumberOfArgsException(cliCommand, 1);
        }
        if(cmdComponents.size() != cliCommand.regExArgValidations.length){
            throw new ToyRobotCommandParseInvalidNumberOfArgsException(cliCommand, cmdComponents.size());
        }
        for(int i = 0; i < cliCommand.regExArgValidations.length; ++i){
            if(!cmdComponents.get(i).matches(cliCommand.regExArgValidations[i].regex)){
                throw new ToyRobotCommandParseInvalidArgumentException(command, i, cmdComponents.get(i), cliCommand.regExArgValidations[i]);
            }
        }
        return new ArrayList<String>(cmdComponents);
    }

    public static List<String> getStripedAndCleanCommandComponents(String command) {
        return new ArrayList<String>(Arrays.asList(command.toLowerCase().replace(',', ' ').trim().split("\\s+")));
    }

    public static String commandsToString(){
        String output = "Toy Robot Commands";
        for(CliCommand cliCommand : CliCommand.values()){
            output += String.format("\n%3s   %-8s %s","-"+ cliCommand.flag, cliCommand.name, cliCommand.description);
        }
        return output;
    }

    public static boolean isInputEmpty(String input) {
        return input.matches("\\s++")||input.isEmpty();
    }
}