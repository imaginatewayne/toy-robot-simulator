package me.wayne.allan.cli.clisessionrunner;

import me.wayne.allan.cli.CliOptions;
import org.apache.commons.cli.CommandLine;


public class CliSessionRunnerFactory{

    public static Class getCliSessionRunnerClass(CommandLine commandLine){
        if(commandLine.hasOption(CliOptions.HELP)){
            return HelpCliSessionRunner.class;
        }
        if(commandLine.hasOption(CliOptions.INTERACTIVE)){
            return InteractiveCliSessionRunner.class;
        }
        if(commandLine.hasOption(CliOptions.FILE)){
            return FileCliSessionRunner.class;
        }
        if(commandLine.hasOption(CliOptions.ROBOT_COMMANDS)){
            return RobotCommandCliSessionRunner.class;
        }
        return HelpCliSessionRunner.class;
    }
}
