package me.wayne.allan.cli;

import me.wayne.allan.cli.exceptions.ToyRobotCommandParseUnknownCommandException;

import java.util.HashMap;
import java.util.Map;

public class CliCommandFactory {
    private static Map<String,CliCommand> CLI_COMMANDS_BY_NAME = new HashMap<String, CliCommand>();
    private static Map<String,CliCommand> CLI_COMMANDS_BY_FLAG = new HashMap<String, CliCommand>();

    static {
        for(CliCommand cliCommand : CliCommand.values()){
            CLI_COMMANDS_BY_NAME.put(cliCommand.name, cliCommand);
            CLI_COMMANDS_BY_FLAG.put("-" + cliCommand.flag, cliCommand);
        }
    }

    public static CliCommand getCliCommand(String cliCommandName) throws ToyRobotCommandParseUnknownCommandException {
        CliCommand parsed = CLI_COMMANDS_BY_FLAG.get(cliCommandName);
        if(parsed == null){
            parsed = CLI_COMMANDS_BY_NAME.get(cliCommandName);
        }
        if(parsed == null){
            throw new ToyRobotCommandParseUnknownCommandException(cliCommandName);
        }
        return parsed;
    }
}
