package me.wayne.allan.cli.exceptions;

/**
 * Created by wayne on 12/07/2014.
 */
public class ToyRobotCommandParseUnknownCommandException extends Exception {
    public ToyRobotCommandParseUnknownCommandException(String command) {
        super(String.format("The following command %s is not recognised.", command));
    }
}
