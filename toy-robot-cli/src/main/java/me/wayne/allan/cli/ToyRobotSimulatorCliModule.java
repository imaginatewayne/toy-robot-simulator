package me.wayne.allan.cli;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Names;
import me.wayne.allan.cli.clisessionrunner.CliSessionRunner;
import me.wayne.allan.cli.clisessionrunner.CliSessionRunnerFactory;
import me.wayne.allan.toyrobotlogic.*;
import org.apache.commons.cli.CommandLine;

public class ToyRobotSimulatorCliModule extends AbstractModule {

    private CommandLine cl;
    public ToyRobotSimulatorCliModule(CommandLine cl) {
        this.cl = cl;
    }

    @Override
    protected void configure() {
        bind(Console.class).to(CliConsole.class);
        bind(Boolean.class)
                .annotatedWith(Names.named("quiteMode"))
                .toInstance(cl.hasOption(CliOptions.SILENT));
        bind(String.class)
                .annotatedWith(Names.named("filePath"))
                .toInstance(cl.getOptionValue(CliOptions.FILE));
        bind(CliSessionRunner.class).to(CliSessionRunnerFactory.getCliSessionRunnerClass(cl));
    }

    @Provides
    public CliSession getCliSession(ToyRobotSimulator trs, Console console){
        return  new CliSession(trs, console);
    }

    @Provides
    public Table getTable(){
        return new InMemoryTable(0,4,0,4);
    }

    @Provides
    public ToyRobotState getToyRobotState(){
        return new InMemoryToyRobotState();
    }

    @Provides
    public SimulatorState getSimulatorState(ToyRobotState toyRobotState){
        return new InMemorySimulatorState(toyRobotState);
    }

    @Provides
    public ToyRobotSimulator getToyRobotSimulator(SimulatorState simulatorState, Table table){
        return new ToyRobotSimulator(simulatorState,table);
    }
}
