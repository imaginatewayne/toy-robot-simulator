package me.wayne.allan.cli.exceptions;

import me.wayne.allan.cli.CliCommand;
import me.wayne.allan.cli.CliUtils;

/**
 * Created by wayne on 12/07/2014.
 */
public class ToyRobotCommandParseInvalidNumberOfArgsException extends Exception {
    public ToyRobotCommandParseInvalidNumberOfArgsException(CliCommand cliCommand, int inputArgSize) {

        super(String.format("Invalid number of arguments command %s takes %d args you entered %d",
                cliCommand.name,
                (cliCommand.regExArgValidations != null) ? cliCommand.regExArgValidations.length : 0,
                inputArgSize));
    }
}
