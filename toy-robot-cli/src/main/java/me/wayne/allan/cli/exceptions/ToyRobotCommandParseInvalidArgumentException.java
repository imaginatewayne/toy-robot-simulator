package me.wayne.allan.cli.exceptions;

import me.wayne.allan.cli.CliArgType;

/**
 * Created by wayne on 12/07/2014.
 */
public class ToyRobotCommandParseInvalidArgumentException extends Exception {
    public ToyRobotCommandParseInvalidArgumentException(String command, int i, String arg, CliArgType regExArgValidation) {
        super(String.format("The value \"%s\" is not of type %s for arg %d of command \"%s\"",arg,regExArgValidation.type,i+1,command));
    }
}
