package me.wayne.allan.cli;

public enum CliCommand {
    PLACE("p", "place", "Places the robot on the table at an x,y coordinate facing a compass direction of NORTH, SOUTH, EAST or WEST \"place x y facing\"",
            CliArgType.INT,
            CliArgType.INT,
            CliArgType.FACING),
    MOVE("m", "move", "Moves the robot 1 unit in the direction it's facing."),
    LEFT("l", "left", "Rotates the robot left to a new facing."),
    RIGHT("r", "right", "Rotates the robot right to a new facing."),
    REPORT("?", "report", "Report on the robot's current position and facing."),
    HELP("h", "help", "Get help on what to do next."),
    EXIT("q", "quit", "Quits the simulator");

    public String flag;
    public String name;
    public String description;
    public CliArgType[] regExArgValidations;

    CliCommand(String flag, String name, String description, CliArgType... regExArgValidations) {
        this.flag = flag;
        this.name = name;
        this.description = description;
        this.regExArgValidations = regExArgValidations;
    }
}

