package me.wayne.allan.cli.clisessionrunner;

import com.google.inject.Inject;
import me.wayne.allan.cli.CliUtils;
import me.wayne.allan.cli.Console;

public class RobotCommandCliSessionRunner implements CliSessionRunner {

    private Console console;

    @Inject
    public RobotCommandCliSessionRunner(Console console) {
        this.console = console;
    }

    @Override
    public void run() {
        console.write(CliUtils.commandsToString());
    }
}
