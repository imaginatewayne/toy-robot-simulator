package me.wayne.allan.cli;

import org.apache.commons.cli.Options;

public class CliOptions {
    public static final String HELP = "h";
    public static final String FILE = "f";
    public static final String INTERACTIVE = "i";
    public static final String ROBOT_COMMANDS = "c";
    public static final String SILENT = "s";

    private static Options cliOptions;

    static {
        cliOptions = new Options();
        cliOptions.addOption(CliOptions.FILE, "file", true, "Specify a text file with a list of commands acceptable commands");
        cliOptions.addOption(CliOptions.HELP, "help", false, "Prints this message");
        cliOptions.addOption(CliOptions.INTERACTIVE, "interactive", false, "Specify commands on the console with interactive mode");
        cliOptions.addOption(CliOptions.ROBOT_COMMANDS, "robot-commands", false, "Lists available robot commands the system can execute");
        cliOptions.addOption(CliOptions.SILENT, "silent", false, "Sets the output to silent mode");
    }

    public static Options getCliOptions(){
        return cliOptions;
    }
}
