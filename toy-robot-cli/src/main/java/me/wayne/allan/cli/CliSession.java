package me.wayne.allan.cli;

import com.google.inject.Inject;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidArgumentException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseUnknownCommandException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidNumberOfArgsException;

import me.wayne.allan.toyrobotlogic.FacingParseException;
import me.wayne.allan.toyrobotlogic.ToyRobotSimulator;

import java.util.List;

/**
 * Created by wayne on 12/07/2014.
 */
public class CliSession{

    private final ToyRobotSimulator trs;
    private final Console console;

    @Inject
    public CliSession(ToyRobotSimulator trs, Console console) {
        this.trs = trs;
        this.console = console;
    }

    public void runCommand(String input) throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException, ToyRobotCommandParseInvalidArgumentException, FacingParseException {
        if(CliUtils.isInputEmpty(input)) return;
        List<String> cmdComponents = CliUtils.getStripedAndCleanCommandComponents(input);
        CliCommand cliCommand = CliCommandFactory.getCliCommand(cmdComponents.get(CliUtils.COMMAND_POSITION));
        String output;
        switch (cliCommand){
            case EXIT: console.exit("Good Bye");
            case HELP: output = CliUtils.commandsToString(); break;
            default  : output = trs.run(
                    ToyRobotCommandFactory.getCommand(
                            cliCommand,
                            CliUtils.parseArgs(cliCommand, cmdComponents)
                    )
            ).getMessage();
        }
        console.write(output);
    }
}
