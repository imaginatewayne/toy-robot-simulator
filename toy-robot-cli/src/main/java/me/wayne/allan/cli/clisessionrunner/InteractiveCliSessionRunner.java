package me.wayne.allan.cli.clisessionrunner;

import com.google.inject.Inject;
import me.wayne.allan.cli.CliSession;
import me.wayne.allan.cli.Console;

import java.util.Scanner;

/**
 * Created by wayne on 12/07/2014.
 */
public class InteractiveCliSessionRunner implements CliSessionRunner {

    private CliSession cliSession;
    private Console console;

    @Inject
    public InteractiveCliSessionRunner(CliSession cliSession, Console console) {
        this.cliSession = cliSession;
        this.console = console;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while(true){
            try {
                cliSession.runCommand(scanner.nextLine());
            } catch (Exception e) {
                console.error(e.getMessage());
            }
        }
    }
}
