package me.wayne.allan.cli;

import com.google.inject.Guice;
import com.google.inject.Injector;
import me.wayne.allan.cli.clisessionrunner.CliSessionRunner;
import org.apache.commons.cli.*;

/**
 * Created by wayne on 10/07/2014.
 */
public class ToyRobotSimulatorCli {

    public static void main(String[] args) {
        try {
            CommandLine cl = new PosixParser().parse(CliOptions.getCliOptions(), args);

            Injector injector = Guice.createInjector(new ToyRobotSimulatorCliModule(cl));

            Console console= injector.getInstance(Console.class);

            console.write("Toy Robot Simulator");

            CliSessionRunner cliRunner = injector.getInstance(CliSessionRunner.class);

            cliRunner.run();

            console.exit("Good Bye");

        } catch (ParseException e) {
            new CliConsole(false).exit(
                    CliConsole.FAILED,
                    "Invalid arguments run with -h (help) flag to view valid arguments"
            );
        }
    }

}
