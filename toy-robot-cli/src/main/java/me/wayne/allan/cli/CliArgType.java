package me.wayne.allan.cli;

public enum CliArgType {
    INT("Integer","^[-]*\\d+$"),
    FACING("Facing","(?i)NORTH|SOUTH|EAST|WEST");

    public String type;
    public String regex;

    private CliArgType(String type, String regex) {
        this.type = type;
        this.regex = regex;
    }
}
