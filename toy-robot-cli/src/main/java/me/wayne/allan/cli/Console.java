package me.wayne.allan.cli;

public interface Console {
    void write(String message, Object... args);

    void error(String message, Object... args);

    void exit(int exitCode, String message, Object... args);

    void exit(String message, Object... args);
}
