package me.wayne.allan.cli;

import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidArgumentException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidNumberOfArgsException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseUnknownCommandException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wayne on 12/07/2014.
 */
public class CliUtilsTest {

    @Test
    public static void shouldValidatePlaceAndArgsCommandFromStringNameWithArgs() throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException, ToyRobotCommandParseInvalidArgumentException {
        ArrayList<String> args = CliUtils.parseArgs(CliCommand.PLACE, new ArrayList<String>() {{
            add("place");
            add("1");
            add("2");
            add("north");
        }});

        Assert.assertEquals(3, args.size());
        Assert.assertEquals("1", args.get(0));
        Assert.assertEquals("2", args.get(1));
        Assert.assertEquals("north", args.get(2));
    }

    @Test
    public static void shouldValidatePlaceAndArgsCommandFromStringNameWithArgsWithMixedCase() throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException, ToyRobotCommandParseInvalidArgumentException {
        ArrayList<String> args = CliUtils.parseArgs(CliCommand.PLACE, new ArrayList<String>() {{
            add("place");
            add("1");
            add("2");
            add("South");
        }});

        Assert.assertEquals(3, args.size());
        Assert.assertEquals("1", args.get(0));
        Assert.assertEquals("2", args.get(1));
        Assert.assertEquals("South", args.get(2));
    }

    @Test
    public static void shouldValidatePlaceAndArgsCommandFromStringNameWithArgsWithDecimal() throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException {
        try {
            CliUtils.parseArgs(CliCommand.PLACE, new ArrayList<String>() {{
                add("place");
                add("1.1");
                add("2");
                add("South");
            }});
            Assert.fail();
        } catch (ToyRobotCommandParseInvalidArgumentException e) {
            Assert.assertEquals(e.getMessage(),
                    "The value \"1.1\" is not of type Integer for arg 1 of command \"place\"");
        }
    }

    @Test
    public static void shouldValidatePlaceAndArgsCommandFromStringNameWithArgsWrongFacing() throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException {
        try {
            CliUtils.parseArgs(CliCommand.PLACE, new ArrayList<String>() {{
                add("place");
                add("1");
                add("2");
                add("UP");
            }});
            Assert.fail();
        } catch (ToyRobotCommandParseInvalidArgumentException e) {
            Assert.assertEquals(e.getMessage(),
                    "The value \"UP\" is not of type Facing for arg 3 of command \"place\"");
        }
    }

    @Test
    public static void shouldValidateMoveAndNullArgsCommandFromString() throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException {
        try {
            Assert.assertNull(CliUtils.parseArgs(CliCommand.MOVE, new ArrayList<String>() {{
                add("move");
            }}));
        } catch (ToyRobotCommandParseInvalidArgumentException e) {
            Assert.fail();
        }
    }

    @Test
    public static void shouldValidateMoveAndInvalidArgsCommandFromString() throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException, ToyRobotCommandParseInvalidArgumentException {
        try {
            Assert.assertNull(CliUtils.parseArgs(CliCommand.MOVE, new ArrayList<String>() {{
                add("move");
                add("blah");
            }}));
            Assert.fail();
        }catch (ToyRobotCommandParseInvalidNumberOfArgsException e){
            Assert.assertEquals(e.getMessage(), "Invalid number of arguments command move takes 0 args you entered 1");
        }

    }

    @Test
    public static void shouldValidatePlaceWithNegativeNumberS() throws ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidNumberOfArgsException, ToyRobotCommandParseInvalidArgumentException {

        ArrayList<String> strings = CliUtils.parseArgs(CliCommand.PLACE, new ArrayList<String>() {{
            add("place");
            add("-1");
            add("1");
            add("north");
        }});
        Assert.assertEquals(strings.get(0), "-1");
        Assert.assertEquals(strings.get(1), "1");
        Assert.assertEquals(strings.get(2), "north");
    }

    @Test
    public void shouldReturnFourCommandComponentsFromSpacedSeparatedString() throws Exception {
        List<String> commandComponents = CliUtils.getStripedAndCleanCommandComponents("place 0 1 north");

        Assert.assertEquals(commandComponents.get(0),"place");
        Assert.assertEquals(commandComponents.get(1),"0");
        Assert.assertEquals(commandComponents.get(2),"1");
        Assert.assertEquals(commandComponents.get(3),"north");
    }

    @Test
    public void shouldReturnFourLowerCommandComponentsFromSpacedSeparatedString() throws Exception {
        List<String> commandComponents = CliUtils.getStripedAndCleanCommandComponents("PLACE 0 1 NORTH");

        Assert.assertEquals(commandComponents.get(0),"place");
        Assert.assertEquals(commandComponents.get(1),"0");
        Assert.assertEquals(commandComponents.get(2),"1");
        Assert.assertEquals(commandComponents.get(3),"north");
    }

    @Test
    public void shouldReturnFourCommandComponentsFromSpacedSeparatedStringWithExcessWhiteSpace() throws Exception {
        List<String> commandComponents = CliUtils.getStripedAndCleanCommandComponents("place 0 1 north  \t\n");

        Assert.assertEquals(commandComponents.get(0),"place");
        Assert.assertEquals(commandComponents.get(1),"0");
        Assert.assertEquals(commandComponents.get(2),"1");
        Assert.assertEquals(commandComponents.get(3),"north");
    }

    @Test
    public void shouldReturnFourCommandComponentsFromCommaSeparatedString() throws Exception {
        List<String> commandComponents = CliUtils.getStripedAndCleanCommandComponents("place,0,1,north");

        Assert.assertEquals(commandComponents.get(0),"place");
        Assert.assertEquals(commandComponents.get(1),"0");
        Assert.assertEquals(commandComponents.get(2),"1");
        Assert.assertEquals(commandComponents.get(3),"north");
    }

    @Test
    public void shouldReturnFourCommandComponentsFromCommaSeparatedStringWithExcess() throws Exception {
        List<String> commandComponents = CliUtils.getStripedAndCleanCommandComponents("place,0,1,north,");

        Assert.assertEquals(commandComponents.get(0),"place");
        Assert.assertEquals(commandComponents.get(1),"0");
        Assert.assertEquals(commandComponents.get(2),"1");
        Assert.assertEquals(commandComponents.get(3),"north");
    }

    @Test
    public void shouldOutputCorrectCommandsToString() throws Exception {
        Assert.assertEquals(CliUtils.commandsToString(),
                "Toy Robot Commands\n" +
                " -p   place    Places the robot on the table at an x,y coordinate facing a compass direction of NORTH, SOUTH, EAST or WEST \"place x y facing\"\n" +
                " -m   move     Moves the robot 1 unit in the direction it's facing.\n" +
                " -l   left     Rotates the robot left to a new facing.\n" +
                " -r   right    Rotates the robot right to a new facing.\n" +
                " -?   report   Report on the robot's current position and facing.\n" +
                " -h   help     Get help on what to do next.\n" +
                " -q   quit     Quits the simulator"
        );
    }

    @Test
    public void shouldReturnTrueForAnEmptyString() throws Exception {
        Assert.assertTrue(CliUtils.isInputEmpty(""));
    }

    @Test
    public void shouldReturnTrueForAnWhiteSpaceString() throws Exception {
        Assert.assertTrue(CliUtils.isInputEmpty("\n\t "));
    }
}
