package me.wayne.allan.cli;

import java.util.ArrayList;
import java.util.List;

public class TestConsole implements Console {

    private List<String> writes = new ArrayList<String>();
    private List<String> errors = new ArrayList<String>();
    private String exitMessage = "";
    private int exitCode = 0;


    @Override
    public void write(String message, Object... args) {
        writes.add(String.format(message,args));
    }

    @Override
    public void error(String message, Object... args) {
        errors.add(String.format(message,args));
    }

    @Override
    public void exit(int exitCode, String message, Object... args) {
        this.exitCode = exitCode;
        this.exitMessage = String.format(message,args);
    }

    @Override
    public void exit(String message, Object... args) {
        this.exitMessage = String.format(message,args);
    }

    public List<String> getWrites() {
        return writes;
    }

    public List<String> getErrors() {
        return errors;
    }

    public String getExitMessage() {
        return exitMessage;
    }

    public int getExitCode() {
        return exitCode;
    }
}
