package me.wayne.allan.cli;

import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidArgumentException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseInvalidNumberOfArgsException;
import me.wayne.allan.cli.exceptions.ToyRobotCommandParseUnknownCommandException;
import me.wayne.allan.toyrobotlogic.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CliSessionTest {
    @Test
    public void shouldRunHelpCommand() throws Exception {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                    new InMemorySimulatorState(new InMemoryToyRobotState()),
                    new InMemoryTable(0,5,0,5)
                ),
                console
        );

        cliSession.runCommand("help");
        Assert.assertEquals(console.getWrites().toString(),
                "[Toy Robot Commands\n" +
                " -p   place    Places the robot on the table at an x,y coordinate facing a compass direction of NORTH, SOUTH, EAST or WEST \"place x y facing\"\n" +
                " -m   move     Moves the robot 1 unit in the direction it's facing.\n" +
                " -l   left     Rotates the robot left to a new facing.\n" +
                " -r   right    Rotates the robot right to a new facing.\n" +
                " -?   report   Report on the robot's current position and facing.\n" +
                " -h   help     Get help on what to do next.\n" +
                " -q   quit     Quits the simulator]");
    }

    @Test
    public void shouldRunPlaceCommand() throws Exception {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                        new InMemorySimulatorState(new InMemoryToyRobotState()),
                        new InMemoryTable(0,5,0,5)
                ),
                console
        );

        cliSession.runCommand("place 0 0 north");

        Assert.assertEquals(console.getWrites().get(0),
                "Successfully placed the Toy Robot at 0,0 facing north");
    }

    @Test
    public void shouldRunMoveCommand() throws Exception {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                        new InMemorySimulatorState(new InMemoryToyRobotState()),
                        new InMemoryTable(0,5,0,5)
                ),
                console
        );

        cliSession.runCommand("place 0 0 north");
        cliSession.runCommand("move");

        Assert.assertEquals(console.getWrites().get(1),
                "Successfully moved the Toy Robot north from 0,0 to 0,1");
    }

    @Test
    public void shouldRunLeftCommand() throws Exception {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                        new InMemorySimulatorState(new InMemoryToyRobotState()),
                        new InMemoryTable(0,5,0,5)
                ),
                console
        );

        cliSession.runCommand("place 0 0 north");
        cliSession.runCommand("left");

        Assert.assertEquals(console.getWrites().get(1),
                "Successfully turned the Toy Robot left from north to west");
    }

    @Test
    public void shouldRunRightCommand() throws Exception {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                        new InMemorySimulatorState(new InMemoryToyRobotState()),
                        new InMemoryTable(0,5,0,5)
                ),
                console
        );

        cliSession.runCommand("place 0 0 north");
        cliSession.runCommand("right");

        Assert.assertEquals(console.getWrites().get(1),
                "Successfully turned the Toy Robot right from north to east");
    }

    @Test
    public void shouldThrowToyRobotCommandParseUnknownCommandExceptionForInvalidCommandAfterPlace() {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                        new InMemorySimulatorState(new InMemoryToyRobotState()),
                        new InMemoryTable(0,5,0,5)
                ),
                console
        );

        try {
            cliSession.runCommand("place 0 0 north");
            cliSession.runCommand("foo");
            Assert.fail("Command 'foo' should fail.");
        } catch (ToyRobotCommandParseUnknownCommandException e) {
            Assert.assertEquals(e.getMessage(),"The following command foo is not recognised.");
        } catch (ToyRobotCommandParseInvalidNumberOfArgsException e) {
           Assert.fail(e.getMessage());
        } catch (ToyRobotCommandParseInvalidArgumentException e) {
            Assert.fail(e.getMessage());
        } catch (FacingParseException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void shouldThrowToyRobotCommandParseUnknownCommandExceptionForInvalidCommandBeforePlace() {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                        new InMemorySimulatorState(new InMemoryToyRobotState()),
                        new InMemoryTable(0,5,0,5)
                ),
                console
        );

        try {
            cliSession.runCommand("foo");
            Assert.fail("Command 'foo' should fail.");
        } catch (ToyRobotCommandParseUnknownCommandException e) {
            Assert.assertEquals(e.getMessage(),"The following command foo is not recognised.");
        } catch (ToyRobotCommandParseInvalidNumberOfArgsException e) {
            Assert.fail(e.getMessage());
        } catch (ToyRobotCommandParseInvalidArgumentException e) {
            Assert.fail(e.getMessage());
        } catch (FacingParseException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void shouldReturnSuggestedCommandWhenRunningAValidCommandBeforePlaceCommand() throws ToyRobotCommandParseInvalidNumberOfArgsException, ToyRobotCommandParseUnknownCommandException, ToyRobotCommandParseInvalidArgumentException, FacingParseException {
        TestConsole console = new TestConsole();
        CliSession cliSession = new CliSession(
                new ToyRobotSimulator(
                        new InMemorySimulatorState(new InMemoryToyRobotState()),
                        new InMemoryTable(0,5,0,5)
                ),
                console
        );

        cliSession.runCommand("move");
        Assert.assertEquals(console.getWrites().get(0),"Run the command place(x,y,facing) to get started simulating a toy robot.");
    }
}
