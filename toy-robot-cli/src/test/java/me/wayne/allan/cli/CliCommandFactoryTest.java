package me.wayne.allan.cli;

import me.wayne.allan.cli.exceptions.ToyRobotCommandParseUnknownCommandException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CliCommandFactoryTest {
    @Test
    public static void shouldValidateMoveCommandFromStringName() throws ToyRobotCommandParseUnknownCommandException {
        CliCommand cliCommand = CliCommandFactory.getCliCommand("move");
        Assert.assertEquals(cliCommand, CliCommand.MOVE);
    }

    @Test
    public static void shouldNotValidateUnknownCommandFromStringName(){
        try {
            CliCommandFactory.getCliCommand("foo");
            Assert.fail();
        } catch (ToyRobotCommandParseUnknownCommandException e) {
            Assert.assertEquals(e.getMessage(),"The following command foo is not recognised.");
        }
    }

    @Test
    public static void shouldValidateMoveCommandFromStringFlag() throws ToyRobotCommandParseUnknownCommandException {
        CliCommand cliCommand = CliCommandFactory.getCliCommand("-m");
        Assert.assertEquals(cliCommand, CliCommand.MOVE);
    }

    @Test
    public static void shouldNotValidateUnknownCommandFromStringFlag(){
        try {
            CliCommandFactory.getCliCommand("-x");
            Assert.fail();
        } catch (ToyRobotCommandParseUnknownCommandException e) {
            Assert.assertEquals(e.getMessage(), "The following command -x is not recognised.");
        }
    }

    @Test
    public static void shouldNotValidateNullCommand(){
        try {
            CliCommandFactory.getCliCommand(null);
            Assert.fail();
        } catch (ToyRobotCommandParseUnknownCommandException e) {
            Assert.assertEquals(e.getMessage(), "The following command null is not recognised.");
        }
    }

    @Test
    public static void shouldValidatePlaceCommandFromStringNameWithArgs() throws ToyRobotCommandParseUnknownCommandException {
        CliCommand cliCommand = CliCommandFactory.getCliCommand("place");
        Assert.assertEquals(cliCommand, CliCommand.PLACE);
    }


}
