package me.wayne.allan.cli;

import me.wayne.allan.toyrobotlogic.command.*;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class ToyRobotCommandFactoryTest {

    @Test
    public void shouldGetPlaceToyRobotCommand() throws Exception {
        ToyRobotCommand command = ToyRobotCommandFactory.getCommand(CliCommand.PLACE,
                new ArrayList<String>() {{
                    add("0");
                    add("1");
                    add("north");
                }}
        );
        Assert.assertTrue(command instanceof PlaceToyRobotCommand);
    }

    @Test
    public void shouldGetMoveToyRobotCommand() throws Exception {
        Assert.assertTrue(ToyRobotCommandFactory.getCommand(CliCommand.MOVE, null) instanceof MoveToyRobotCommand);
    }

    @Test
    public void shouldGetReportToyRobotCommand() throws Exception {
        Assert.assertTrue(ToyRobotCommandFactory.getCommand(CliCommand.REPORT, null) instanceof ReportToyRobotCommand);
    }

    @Test
    public void shouldGetLeftToyRobotCommand() throws Exception {
        Assert.assertTrue(ToyRobotCommandFactory.getCommand(CliCommand.LEFT, null) instanceof TurnToyRobotCommand);
    }

    @Test
    public void shouldGetRightToyRobotCommand() throws Exception {
        Assert.assertTrue(ToyRobotCommandFactory.getCommand(CliCommand.RIGHT, null) instanceof TurnToyRobotCommand);
    }

    @Test
    public void shouldGetReportToyRobotCommandWhenGivenTheUnsupportedHelpCommand() throws Exception {
        Assert.assertTrue(ToyRobotCommandFactory.getCommand(CliCommand.HELP, null) instanceof ReportToyRobotCommand);
    }

    @Test
    public void shouldGetReportToyRobotCommandWhenGivenTheUnsupportedExitCommand() throws Exception {
        Assert.assertTrue(ToyRobotCommandFactory.getCommand(CliCommand.EXIT, null) instanceof ReportToyRobotCommand);
    }
}
