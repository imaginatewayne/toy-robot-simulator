package me.wayne.allan.cli;

import org.apache.commons.cli.Options;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CliOptionsTest {
    @Test
    public void shouldGetTheListOfCliOptionsWithAllOptionsPresent() throws Exception {
        Options cliOptions = CliOptions.getCliOptions();

        Assert.assertTrue(cliOptions.hasOption(CliOptions.HELP));
        Assert.assertTrue(cliOptions.hasOption(CliOptions.FILE));
        Assert.assertTrue(cliOptions.hasOption(CliOptions.INTERACTIVE));
        Assert.assertTrue(cliOptions.hasOption(CliOptions.ROBOT_COMMANDS));
        Assert.assertTrue(cliOptions.hasOption(CliOptions.SILENT));
    }
}
