package me.wayne.allan.cli;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import me.wayne.allan.toyrobotlogic.*;

public class ToyRobotSimulatorCliTestModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Console.class).to(TestConsole.class);
    }

    @Provides
    public CliSession getCliSession(ToyRobotSimulator trs, Console console){
        return  new CliSession(trs, console);
    }

    @Provides
    public Table getTable(){
        return new InMemoryTable(0,4,0,4);
    }

    @Provides
    public ToyRobotState getToyRobotState(){
        return new InMemoryToyRobotState();
    }

    @Provides
    public SimulatorState getSimulatorState(ToyRobotState toyRobotState){
        return new InMemorySimulatorState(toyRobotState);
    }

    @Provides
    public ToyRobotSimulator getToyRobotSimulator(SimulatorState simulatorState, Table table){
        return new ToyRobotSimulator(simulatorState,table);
    }
}
