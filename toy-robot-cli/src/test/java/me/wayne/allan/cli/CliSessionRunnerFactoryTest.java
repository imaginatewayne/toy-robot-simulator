package me.wayne.allan.cli;

import me.wayne.allan.cli.clisessionrunner.*;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CliSessionRunnerFactoryTest {

    @Test
    public void shouldReturnHelpCliRunnerWhenHelpOptionIsPresent() throws Exception {
        CommandLine commandLine = getNewCommandLine(new String[]{"-h"});

        Assert.assertTrue(commandLine.hasOption("-h"));
        Assert.assertEquals(CliSessionRunnerFactory.getCliSessionRunnerClass(commandLine), HelpCliSessionRunner.class);
    }

    @Test
    public void shouldReturnHelpCliRunnerWhenNoOptionIsPresent() throws Exception {
        CommandLine commandLine = getNewCommandLine(new String[]{""});

        Assert.assertFalse(commandLine.hasOption("-h"));
        Assert.assertEquals(CliSessionRunnerFactory.getCliSessionRunnerClass(commandLine), HelpCliSessionRunner.class);
    }

    @Test
    public void shouldReturnInteractiveCliRunnerWhenInteractiveOptionIsPresent() throws Exception {
        CommandLine commandLine = getNewCommandLine(new String[]{"-i"});

        Assert.assertTrue(commandLine.hasOption("-i"));
        Assert.assertEquals(CliSessionRunnerFactory.getCliSessionRunnerClass(commandLine), InteractiveCliSessionRunner.class);
    }

    @Test
    public void shouldReturnFileCliRunnerWhenFileOptionIsPresent() throws Exception {
        CommandLine commandLine = getNewCommandLine(new String[]{"-f","--file foo.txt"});

        Assert.assertTrue(commandLine.hasOption("-f"));
        Assert.assertEquals(CliSessionRunnerFactory.getCliSessionRunnerClass(commandLine), FileCliSessionRunner.class);
    }

    @Test
    public void shouldReturnRobotCommandCliRunnerWhenCommandOptionIsPresent() throws Exception {
        CommandLine commandLine = getNewCommandLine(new String[]{"-c"});

        Assert.assertTrue(commandLine.hasOption("-c"));
        Assert.assertEquals(CliSessionRunnerFactory.getCliSessionRunnerClass(commandLine), RobotCommandCliSessionRunner.class);
    }

    private CommandLine getNewCommandLine(final String... args) throws ParseException {
        return new org.apache.commons.cli.Parser() {
            @Override
            protected String[] flatten(Options options, String[] strings, boolean b) {
                return args;
            }
        }.parse(CliOptions.getCliOptions(),args);
    }
}
