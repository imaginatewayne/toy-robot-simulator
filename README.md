# README #

This repository implements the Toy Robot Simulator.

## Getting Started ##

### Dependencies ###
* You will need to install the following:
    * [Java jdk 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
    * [Maven 3.2.2](http://maven.apache.org/download.cgi)
* Setup your path to have the following values
    * JAVA_HOME = /java/location/java/jdk1.8.0/bin
    * MAVEN_HOME = /maven/location/maven/apache-maven-3.2.2/

To check that Java and Maven are setup correctly you can test them by trying to run the following command:
```robotframework
    mvn -v
```
This will display the installed versions of java and maven.

### Build and Unit Test the Application ###

To build and unit test the application from the root directory run the following command:
```robotframework
    mvn package
```

### Functional Testing of the Application ###
To run the functional test from the root directory run the following command:
```robotframework
    mvn failsafe:integration-test
```
Note - If you are running tests in Intellij be sure the setup your [JUnit working directory](http://stackoverflow.com/questions/7461987/intellij-problem-withget-current-working-directory).

### Code Coverage ###
To run code coverage make sure that you have built and packaged the application, then run the command below. You can view the reports in each module's target directory.
```robotframework
    mvn jacoco:report
```


## Running the Cli Application ##
The application will be compiled in the 'target' folder with the name 'trs.jar'. You can run the application with the following standard java commands.
```robotframework
    java -jar ./toy-robot-cli/target/trs.jar
```
### Cli Commands ###
Run the jar with any of the following commands.
```robotframework
    usage: trs [-c] [-f <arg>] [-h] [-i] [-s]
     -c,--robot-commands   Lists available robot commands the system can execute
     -f,--file <arg>       Specify a text file with a list of commands acceptable commands
     -h,--help             Prints this message
     -i,--interactive      Specify commands on the console with interactive mode
     -s,--silent           Sets the output to silent mode
```
### Toy Robot Commands ###
The following commands operate the robot on the table.
```robotframework
     -p   place    Places the robot on the table at an x,y coordinate facing a compass direction of NORTH, SOUTH, EAST or WEST "place x y facing"
     -m   move     Moves the robot 1 unit in the direction it's facing.
     -l   left     Rotates the robot left to a new facing.
     -r   right    Rotates the robot right to a new facing.
     -?   report   Report on the robot's current position and facing.
     -h   help     Get help on what to do next.
     -q   quit     Quits the simulator
```

### Examples ###

You can run any of the sample scripts in the 'sample-script' folder eg.

Running a file with commands.
```robotframework
    java -jar ./toy-robot-cli/target/trs.jar -f ./toy-robot-functional-tests/src/test/resources/me/wayne/allan/test-scripts/example_test_data_a.txt
```

Running commands in interactive mode
```robotframework
    java -jar ./toy-robot-cli/target/trs.jar -i
    place 1,2,north
    move
    move
    left
    move
    report
```