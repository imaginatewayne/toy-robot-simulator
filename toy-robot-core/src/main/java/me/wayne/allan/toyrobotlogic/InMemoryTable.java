package me.wayne.allan.toyrobotlogic;

/**
 * Created by wayne on 8/07/2014.
 */
public class InMemoryTable implements Table {

    private int minY;
    private int maxY;
    private int minX;
    private int maxX;

    public InMemoryTable(int minY, int maxY, int minX, int maxX) {
        this.minY = minY;
        this.maxY = maxY;
        this.minX = minX;
        this.maxX = maxX;
    }

    @Override
    public int getMinY() {
        return minY;
    }

    @Override
    public int getMaxY() {
        return maxY;
    }

    @Override
    public int getMinX() {
        return minX;
    }

    @Override
    public int getMaxX() {
        return maxX;
    }
}
