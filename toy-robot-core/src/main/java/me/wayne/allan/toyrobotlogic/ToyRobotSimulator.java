package me.wayne.allan.toyrobotlogic;

import me.wayne.allan.toyrobotlogic.command.CommandResult;
import me.wayne.allan.toyrobotlogic.command.ToyRobotCommand;

/**
 * Created by wayne on 8/07/2014.
 */
public class ToyRobotSimulator {

    private SimulatorState simulatorState;
    private Table table;

    public ToyRobotSimulator(SimulatorState simulatorState, Table table) {
        this.simulatorState = simulatorState;
        this.table = table;
    }

    public CommandResult run(ToyRobotCommand toyRobotCommand){
        return toyRobotCommand.run(this.simulatorState, this.table);
    }
}
