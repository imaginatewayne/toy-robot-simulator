package me.wayne.allan.toyrobotlogic;

public class TableUtils {
    public static boolean validCoordinates(Table table,int x, int y) {
        return (x >= table.getMinX() && x <= table.getMaxX() &&
                y >= table.getMinY() && y <= table.getMaxY());
    }
}
