package me.wayne.allan.toyrobotlogic;

public interface SimulatorState {
    void setPosition(int x, int y);

    void setFacing(Facing facing);

    void setPlaceCommand();

    Facing getToyRobotFacing();

    int getToyRobotXPosition();

    int getToyRobotYPosition();

    boolean hasToyRobotBeenPlacedOnTable();
}
