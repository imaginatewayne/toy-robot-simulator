package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.*;

public class PlaceToyRobotCommand implements  ToyRobotCommand {

    private final int x;
    private final int y;
    private final Facing facing;

    public PlaceToyRobotCommand(int x, int y, Facing facing) {
        this.x = x;
        this.y = y;
        this.facing = facing;
    }

    @Override
    public CommandResult run(SimulatorState simulatorState, Table table) {
        if(!TableUtils.validCoordinates(table, x, y)){
            return new CommandResult(
                    "Invalid coordinates %d,%d please enter a values of x between %d to %d and y between %d to %d",
                    x,
                    y,
                    table.getMinX(),
                    table.getMaxX(),
                    table.getMinY(),
                    table.getMaxY()
            );
        }
        simulatorState.setPosition(x,y);
        simulatorState.setFacing(facing);
        simulatorState.setPlaceCommand();
        return new CommandResult(
                "Successfully placed the Toy Robot at %d,%d facing %s",
                x,
                y,
                facing.getName()
        );
    }
}
