package me.wayne.allan.toyrobotlogic.command;

public class CommandUtils {

    public static CommandResult NOT_PLACED_COMMAND_RESULT  = new CommandResult("Run the command place(x,y,facing) to get started simulating a toy robot.");
}
