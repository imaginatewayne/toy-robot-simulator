package me.wayne.allan.toyrobotlogic;

/**
 * Created by wayne on 8/07/2014.
 */
public class InMemoryToyRobotState implements ToyRobotState {
    private Facing facing;

    @Override
    public Facing getFacing() {
        return facing;
    }

    @Override
    public void setFacing(Facing facing) {
        this.facing = facing;
    }
}
