package me.wayne.allan.toyrobotlogic;

/**
 * Created by wayne on 8/07/2014.
 */

public class Facing {

    private final String name;
    private final int xModifier;
    private final int yModifier;

    Facing(String name, int xModifier, int yModifier){
        this.xModifier = xModifier;
        this.yModifier = yModifier;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getXModifier() {
        return xModifier;
    }

    public int getYModifier() {
        return yModifier;
    }
}
