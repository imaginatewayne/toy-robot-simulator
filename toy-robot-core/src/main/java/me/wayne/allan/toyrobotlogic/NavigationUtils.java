package me.wayne.allan.toyrobotlogic;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wayne on 10/07/2014.
 */
public class NavigationUtils {
    public enum Turn {
        LEFT, RIGHT;

        public String toString(){
            return this.name().toLowerCase();
        }
    }

    public static final Turn LEFT = Turn.LEFT;
    public static final Turn RIGHT = Turn.RIGHT;

    private static Map<Facing, TurnDirections> turnDirectionsMap = new HashMap<Facing, TurnDirections>() {{
        put(FacingFactory.NORTH, new TurnDirections(FacingFactory.WEST, FacingFactory.EAST));
        put(FacingFactory.EAST, new TurnDirections(FacingFactory.NORTH, FacingFactory.SOUTH));
        put(FacingFactory.SOUTH, new TurnDirections(FacingFactory.EAST, FacingFactory.WEST));
        put(FacingFactory.WEST, new TurnDirections(FacingFactory.SOUTH, FacingFactory.NORTH));
    }};

    public static Facing turn(Facing currentFacing, Turn turn) {
        TurnDirections turnDirections = turnDirectionsMap.get(currentFacing);
        return (turn == Turn.LEFT) ? turnDirections.getLeft() : turnDirections.getRight();
    }
}