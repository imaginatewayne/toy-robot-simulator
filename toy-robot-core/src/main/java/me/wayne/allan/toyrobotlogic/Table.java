package me.wayne.allan.toyrobotlogic;

public interface Table {
    int getMinY();

    int getMaxY();

    int getMinX();

    int getMaxX();
}
