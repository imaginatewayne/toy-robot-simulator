package me.wayne.allan.toyrobotlogic;

public class FacingFactory {
    public static Facing NORTH = new Facing("north",0,1);
    public static Facing SOUTH = new Facing("south",0,-1);
    public static Facing EAST = new Facing("east",1,0);
    public static Facing WEST = new Facing("west",-1,0);

    public static Facing getFacing(String facingString) throws FacingParseException{
        if("NORTH".equalsIgnoreCase(facingString)){
            return NORTH;
        }
        if("SOUTH".equalsIgnoreCase(facingString)){
            return SOUTH;
        }
        if("EAST".equalsIgnoreCase(facingString)){
            return EAST;
        }
        if("WEST".equalsIgnoreCase(facingString)){
            return WEST;
        }
        throw new FacingParseException(String.format("String '%s' not recognised as Facing try NORTH, SOUTH, EAST, WEST.",facingString));
    }
}
