package me.wayne.allan.toyrobotlogic;

public class FacingParseException extends Exception{
    public FacingParseException(String message) {
        super(message);
    }
}
