package me.wayne.allan.toyrobotlogic;

public interface ToyRobotState {
    Facing getFacing();

    void setFacing(Facing facing);
}
