package me.wayne.allan.toyrobotlogic;

/**
 * Created by wayne on 8/07/2014.
 */
public class InMemorySimulatorState implements SimulatorState {
    private ToyRobotState toyRobotState;
    private int toyRobotXPosition;
    private int toyRobotYPosition;
    private boolean placed = false;

    public InMemorySimulatorState(ToyRobotState toyRobotState) {
        this.toyRobotState = toyRobotState;
    }

    @Override
    public void setPosition(int x, int y) {
        toyRobotXPosition = x;
        toyRobotYPosition = y;
    }

    @Override
    public void setFacing(Facing facing) {
        this.toyRobotState.setFacing(facing);
    }

    @Override
    public void setPlaceCommand() {
        this.placed = true;
    }

    @Override
    public Facing getToyRobotFacing() {
        return toyRobotState.getFacing();
    }

    @Override
    public int getToyRobotXPosition() {
        return toyRobotXPosition;
    }

    @Override
    public int getToyRobotYPosition() {
        return toyRobotYPosition;
    }

    @Override
    public boolean hasToyRobotBeenPlacedOnTable() {
        return placed;
    }
}
