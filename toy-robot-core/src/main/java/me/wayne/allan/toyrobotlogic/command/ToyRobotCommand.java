package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.SimulatorState;
import me.wayne.allan.toyrobotlogic.Table;

public interface ToyRobotCommand {
    CommandResult run(SimulatorState simulatorState,Table table);
}
