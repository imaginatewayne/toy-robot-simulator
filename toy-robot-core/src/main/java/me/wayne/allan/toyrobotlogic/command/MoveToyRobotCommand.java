package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.*;

public class MoveToyRobotCommand implements ToyRobotCommand {



    @Override
    public CommandResult run(SimulatorState simulatorState, Table table) {
        if(!simulatorState.hasToyRobotBeenPlacedOnTable()) {
            return CommandUtils.NOT_PLACED_COMMAND_RESULT;
        }

        Facing toyRobotFacing = simulatorState.getToyRobotFacing();
        int oldX = simulatorState.getToyRobotXPosition();
        int oldY = simulatorState.getToyRobotYPosition();
        int newX = oldX + toyRobotFacing.getXModifier();
        int newY = oldY + toyRobotFacing.getYModifier();

        if(!TableUtils.validCoordinates(table, newX, newY)){
            return new CommandResult(
                    "Failed move: the Toy Robot is facing %s and tried to move of the table from %d,%d to %d,%d",
                    toyRobotFacing.getName(),
                    oldX,
                    oldY,
                    newX,
                    newY
            );
        }

        simulatorState.setPosition(newX,newY);

        return new CommandResult(
                "Successfully moved the Toy Robot %s from %d,%d to %d,%d",
                toyRobotFacing.getName(),
                oldX,
                oldY,
                simulatorState.getToyRobotXPosition(),
                simulatorState.getToyRobotYPosition()
        );
    }
}
