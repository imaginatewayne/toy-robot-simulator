package me.wayne.allan.toyrobotlogic;

public class TurnDirections {

    private Facing left;
    private Facing right;

    TurnDirections(Facing left, Facing right) {
        this.left = left;
        this.right = right;
    }

    public Facing getLeft() {
        return left;
    }

    public Facing getRight() {
        return right;
    }

}
