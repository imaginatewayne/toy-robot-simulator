package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.SimulatorState;
import me.wayne.allan.toyrobotlogic.Table;

public class ReportToyRobotCommand implements ToyRobotCommand {

    @Override
    public CommandResult run(SimulatorState simulatorState, Table table) {
        if(!simulatorState.hasToyRobotBeenPlacedOnTable()) {
            return CommandUtils.NOT_PLACED_COMMAND_RESULT;
        }

        return new CommandResult(
                "Output: %s,%s,%s",
                simulatorState.getToyRobotXPosition(),
                simulatorState.getToyRobotYPosition(),
                simulatorState.getToyRobotFacing().getName()
        );
    }
}
