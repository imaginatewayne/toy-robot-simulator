package me.wayne.allan.toyrobotlogic.command;

/**
 * Created by wayne on 8/07/2014.
 */
public class CommandResult {

    private String message;

    public CommandResult(String messageFormat, Object... args) {
        this.message = String.format(messageFormat,args);
    }

    public String getMessage() {
        return message;
    }
}
