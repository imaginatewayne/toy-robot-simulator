package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.*;

public class TurnToyRobotCommand implements ToyRobotCommand {

    private final NavigationUtils.Turn turn;

    public TurnToyRobotCommand(NavigationUtils.Turn turn) {
        this.turn = turn;
    }

    @Override
    public CommandResult run(SimulatorState simulatorState, Table table) {
        if(!simulatorState.hasToyRobotBeenPlacedOnTable()) {
            return CommandUtils.NOT_PLACED_COMMAND_RESULT;
        }

        Facing oldFacing = simulatorState.getToyRobotFacing();
        Facing newFacing = NavigationUtils.turn(oldFacing, turn);
        simulatorState.setFacing(newFacing);
        return new CommandResult(
                "Successfully turned the Toy Robot %s from %s to %s",
                turn,
                oldFacing.getName(),
                newFacing.getName()
        );
    }
}
