package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TurnToyRobotCommandTest {

    @Test
    public void leftCommandShouldFailAsTheFirstCommand() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new TurnToyRobotCommand(NavigationUtils.Turn.LEFT).run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Run the command place(x,y,facing) to get started simulating a toy robot.");
    }

    @Test
    public void leftCommandShouldFaceTheToyRobotFromNorthToWest() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        InMemoryTable table = new InMemoryTable(0, 4, 0, 4);
        new PlaceToyRobotCommand(0, 0, FacingFactory.NORTH).run(
                simulatorState,
                table
        );

        CommandResult commandResult = new TurnToyRobotCommand(NavigationUtils.Turn.LEFT).run(
                simulatorState,
                table
        );


        Assert.assertEquals(simulatorState.getToyRobotFacing(), FacingFactory.WEST);
        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(commandResult.getMessage(),
                "Successfully turned the Toy Robot left from north to west");
    }
    @Test
    public void rightCommandShouldFaceTheToyRobotFromNorthToEast() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        InMemoryTable table = new InMemoryTable(0, 4, 0, 4);
        new PlaceToyRobotCommand(0, 0, FacingFactory.NORTH).run(
                simulatorState,
                table
        );

        CommandResult commandResult = new TurnToyRobotCommand(NavigationUtils.Turn.RIGHT).run(
                simulatorState,
                table
        );

        Assert.assertEquals(simulatorState.getToyRobotFacing(), FacingFactory.EAST);
        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(commandResult.getMessage(),
                "Successfully turned the Toy Robot right from north to east");
    }
}
