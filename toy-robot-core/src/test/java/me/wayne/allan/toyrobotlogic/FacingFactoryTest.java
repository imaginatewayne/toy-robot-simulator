package me.wayne.allan.toyrobotlogic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FacingFactoryTest {
    @Test
    public void facingLookUpShouldReturnTheCorrectValueForNorth() throws FacingParseException {
        Assert.assertEquals(FacingFactory.NORTH, FacingFactory.getFacing("NORTH"));
    }

    @Test
    public void facingLookUpShouldReturnTheCorrectValueForSouth() throws FacingParseException {
        Assert.assertEquals(FacingFactory.SOUTH, FacingFactory.getFacing("SOUTH"));
    }

    @Test
    public void facingLookUpShouldReturnTheCorrectValueForEast() throws FacingParseException {
        Assert.assertEquals(FacingFactory.EAST, FacingFactory.getFacing("EAST"));
    }

    @Test
    public void facingLookUpShouldReturnTheCorrectValueForWest() throws FacingParseException {
        Assert.assertEquals(FacingFactory.WEST, FacingFactory.getFacing("WEST"));
    }

    @Test
    public void facingLookUpShouldReturnTheCorrectValueForUpperCase() throws FacingParseException {
        Assert.assertEquals(FacingFactory.NORTH, FacingFactory.getFacing("NORTH"));
    }

    @Test
    public void facingLookUpShouldReturnTheCorrectValueForLowerCase() throws FacingParseException {
        Assert.assertEquals(FacingFactory.NORTH, FacingFactory.getFacing("north"));
    }

    @Test
    public void facingLookUpShouldReturnTheCorrectValueForMixedCase() throws FacingParseException {
        Assert.assertEquals(FacingFactory.NORTH, FacingFactory.getFacing("North"));
    }

    @Test
    public void facingLookUpShouldThrowFacingParseExceptionWhenGivenNonMatchingFacingString(){
        try {
            FacingFactory.getFacing("Foo");
            Assert.fail("Should throw exception on failure to getFacing a facing string.");
        } catch(FacingParseException e){
            Assert.assertEquals(e.getMessage(), "String 'Foo' not recognised as Facing try NORTH, SOUTH, EAST, WEST.");
        }
    }
}
