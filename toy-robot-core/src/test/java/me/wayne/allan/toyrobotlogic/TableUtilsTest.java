package me.wayne.allan.toyrobotlogic;

import junit.framework.Assert;
import org.testng.annotations.Test;

public class TableUtilsTest {

    @Test
    public void shouldReturnTrueValidateXYCoordinatesForXOneYTwo() throws Exception {
        Assert.assertTrue(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),1, 2));
    }

    //Positive Corner Tests
    @Test
    public void shouldReturnTrueValidateXYCoordinatesForCornerXZeroYZero() throws Exception {
        Assert.assertTrue(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),0, 0));
    }

    @Test
    public void shouldReturnTrueValidateXYCoordinatesForCornerXZeroYFour() throws Exception {
        Assert.assertTrue(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),0, 4));
    }

    @Test
    public void shouldReturnTrueValidateXYCoordinatesForCornerXFourYFour() throws Exception {
        Assert.assertTrue(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),4, 4));
    }

    @Test
    public void shouldReturnTrueValidateXYCoordinatesForCornerXFourYZero() throws Exception {
        Assert.assertTrue(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),4, 0));
    }

    //Negative Corner Tests
    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXNegativeOneYZero() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),-1, 0));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXZeroYNegativeOne() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),0, -1));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXNegativeOneYNegativeOne() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),-1, -1));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXNegativeOneYFour() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),-1, 4));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXZeroYFive() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),0, 5));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXNegativeOneYFive() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),-1, 5));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXFiveYFour() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),5, 4));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXFourYFive() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),4, 5));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXFiveYFive() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),5, 5));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXFourYNegativeOne() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),4, -1));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXFiveYZero() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),5, 0));
    }

    @Test
    public void shouldReturnFalseValidateXYCoordinatesForCornerXFiveYNegativeOne() throws Exception {
        Assert.assertFalse(TableUtils.validCoordinates(new InMemoryTable(0,4,0,4),5, -1));
    }
}
