package me.wayne.allan.toyrobotlogic;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NavigationUtilsTest {

    @Test
    public void shouldFaceWestWhenTurningLeftFromNorthFacing() throws Exception {
        Assert.assertEquals(FacingFactory.WEST, NavigationUtils.turn(FacingFactory.NORTH, NavigationUtils.LEFT));
    }

    @Test
    public void shouldFaceSouthWhenTurningLeftFromWestFacing() throws Exception {
        Assert.assertEquals(FacingFactory.SOUTH, NavigationUtils.turn(FacingFactory.WEST, NavigationUtils.LEFT));
    }

    @Test
    public void shouldFaceEastWhenTurningLeftFromSouthFacing() throws Exception {
        Assert.assertEquals(FacingFactory.EAST, NavigationUtils.turn(FacingFactory.SOUTH, NavigationUtils.LEFT));
    }

    @Test
    public void shouldFaceNorthWhenTurningLeftFromEastFacing() throws Exception {
        Assert.assertEquals(FacingFactory.NORTH, NavigationUtils.turn(FacingFactory.EAST, NavigationUtils.LEFT));
    }

    @Test
    public void shouldFaceEastWhenTurningRightFromNorthFacing() throws Exception {
        Assert.assertEquals(FacingFactory.EAST, NavigationUtils.turn(FacingFactory.NORTH, NavigationUtils.RIGHT));
    }

    @Test
    public void shouldFaceSouthWhenTurningRightFromWestFacing() throws Exception {
        Assert.assertEquals(FacingFactory.SOUTH, NavigationUtils.turn(FacingFactory.EAST, NavigationUtils.RIGHT));
    }

    @Test
    public void shouldFaceWestWhenTurningRightFromSouthFacing() throws Exception {
        Assert.assertEquals(FacingFactory.WEST, NavigationUtils.turn(FacingFactory.SOUTH, NavigationUtils.RIGHT));
    }

    @Test
    public void shouldFaceNorthWhenTurningRightFromWestFacing() throws Exception {
        Assert.assertEquals(FacingFactory.NORTH, NavigationUtils.turn(FacingFactory.WEST, NavigationUtils.RIGHT));
    }
}
