package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.*;
import me.wayne.allan.toyrobotlogic.command.CommandResult;
import me.wayne.allan.toyrobotlogic.command.PlaceToyRobotCommand;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PlaceToyRobotCommandTest {

    @Test
    public void placeCommandShouldPositionTheRobotAtTheGivenCoordinates() {
        InMemorySimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new PlaceToyRobotCommand(0,0, FacingFactory.NORTH).run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Successfully placed the Toy Robot at 0,0 facing north");
        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(simulatorState.getToyRobotYPosition(), 0);
        Assert.assertEquals(simulatorState.getToyRobotFacing(), FacingFactory.NORTH);
    }

    @Test
    public void placeCommandShouldPositionTheRobotAtTheGivenCoordinatesMaxBoundaries() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new PlaceToyRobotCommand(4,4,FacingFactory.SOUTH).run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Successfully placed the Toy Robot at 4,4 facing south");

        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 4);
        Assert.assertEquals(simulatorState.getToyRobotYPosition(), 4);
        Assert.assertEquals(simulatorState.getToyRobotFacing(), FacingFactory.SOUTH);
    }

    @Test
    public void placeCommandShouldFailWhenPositioningTheRobotAtInvalidNegativeCoordinates() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new PlaceToyRobotCommand(-1,-1,FacingFactory.NORTH).run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Invalid coordinates -1,-1 please enter a values of x between 0 to 4 and y between 0 to 4");
    }

    @Test
    public void placeCommandShouldFailWhenPositioningTheRobotAtInvalidExceedingCoordinates() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new PlaceToyRobotCommand(5,5,FacingFactory.NORTH).run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Invalid coordinates 5,5 please enter a values of x between 0 to 4 and y between 0 to 4");
    }


    @Test
    public void placeCommandShouldSucceedAsTheFirstCommand() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new PlaceToyRobotCommand(3,3,FacingFactory.EAST).run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Successfully placed the Toy Robot at 3,3 facing east");
        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 3);
        Assert.assertEquals(simulatorState.getToyRobotYPosition(), 3);
        Assert.assertEquals(simulatorState.getToyRobotFacing(), FacingFactory.EAST);
    }
}
