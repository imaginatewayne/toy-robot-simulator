package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MoveToyRobotCommandTest {
    @Test
    public void moveCommandShouldFailAsTheFirstCommand() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new MoveToyRobotCommand().run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Run the command place(x,y,facing) to get started simulating a toy robot.");
    }

    @Test
    public void moveCommandShouldMoveToyRobotOnUnit() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        InMemoryTable table = new InMemoryTable(0, 4, 0, 4);
        new PlaceToyRobotCommand(0, 0, FacingFactory.NORTH).run(
                simulatorState,
                table
        );

        CommandResult commandResult = new MoveToyRobotCommand().run(
                simulatorState,
                table
        );

        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(simulatorState.getToyRobotYPosition(), 1);
        Assert.assertEquals(commandResult.getMessage(),
                "Successfully moved the Toy Robot north from 0,0 to 0,1");
    }

    @Test
    public void moveCommandShouldFailWhenRobotTriesToMoveOverEdgeOfTable() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        InMemoryTable table = new InMemoryTable(0, 4, 0, 4);
        new PlaceToyRobotCommand(0, 0, FacingFactory.SOUTH).run(
                simulatorState,
                table
        );

        CommandResult commandResult = new MoveToyRobotCommand().run(
                simulatorState,
                table
        );

        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(simulatorState.getToyRobotYPosition(), 0);
        Assert.assertEquals(commandResult.getMessage(),
                "Failed move: the Toy Robot is facing south and tried to move of the table from 0,0 to 0,-1");
    }

    @Test
    public void moveCommandShouldWorkWithMultipleMovesOfToyRobotOnUnit() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        InMemoryTable table = new InMemoryTable(0, 4, 0, 4);
        new PlaceToyRobotCommand(0, 0, FacingFactory.NORTH).run(
                simulatorState,
                table
        );

        MoveToyRobotCommand moveToyRobotCommand = new MoveToyRobotCommand();
        moveToyRobotCommand.run(simulatorState,table);
        moveToyRobotCommand.run(simulatorState,table);
        CommandResult commandResult = moveToyRobotCommand.run(
                simulatorState,
                table
        );

        Assert.assertEquals(simulatorState.getToyRobotXPosition(), 0);
        Assert.assertEquals(simulatorState.getToyRobotYPosition(), 3);
        Assert.assertEquals(commandResult.getMessage(),
                "Successfully moved the Toy Robot north from 0,2 to 0,3");
    }
}
