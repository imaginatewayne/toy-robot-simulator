package me.wayne.allan.toyrobotlogic.command;

import me.wayne.allan.toyrobotlogic.InMemorySimulatorState;
import me.wayne.allan.toyrobotlogic.InMemoryTable;
import me.wayne.allan.toyrobotlogic.InMemoryToyRobotState;
import me.wayne.allan.toyrobotlogic.SimulatorState;
import me.wayne.allan.toyrobotlogic.command.CommandResult;
import me.wayne.allan.toyrobotlogic.command.ReportToyRobotCommand;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ReportToyRobotCommandTest {

    @Test
    public void reportCommandShouldFailAsTheFirstCommand() {
        SimulatorState simulatorState = new InMemorySimulatorState(new InMemoryToyRobotState());
        CommandResult commandResult = new ReportToyRobotCommand().run(
                simulatorState,
                new InMemoryTable(0,4,0,4)
        );

        Assert.assertEquals(commandResult.getMessage(),
                "Run the command place(x,y,facing) to get started simulating a toy robot.");
    }
}
