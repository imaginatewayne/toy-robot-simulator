# Design
Here are some notes to highlight some of the design decisions I've made in order to demonstrate my skills and understanding. 
  
## Separate Projects
I broke the solution into three projects drawing a line between the logic of the robot and the user interface. 
This also servers to highlight other important aspects of this design such as extensibility of game logic, persistence of game state and console output.

## Game Logic Extensibility 
If so desired additional game logic could be created not only at the toy-robot-core but from out side the produced jar,
for example if there was need for a new move type called "Jump" this could be implemented in the toy-robot-cli with out changing a line
of code in toy-robot-core. This would be useful if the toy-robot-core was a published library that aws used by other projects who had no 
control over the source code.

## Interchangeable State Persistence
At this stage DI isn't implemented in the toy-robot-core, but the code is designed so that with additional effort the objects "State" objects 
could be injectable, this would allow projects using toy-robot-core to add their own form of persistence. For example a web app could back the state with 
DB implementations of the state objects.

## Interchangeable Console
The toy-robot-cli uses an object implementing Console to interact with output to the cli, this can be dependency injected which helps with
making the code more testable because output can be directed to an in memory list rather than the console. An example of this is TestConsole.java

## Functional Test
I have included some basic BDD tests to demonstrate my understanding of BDD and the Cucumber framework.

## Dependency Injection
I have implemented DI in only toy-robot-cli, this demonstrates how to interact and wire up objects with libraries that don't implement DI.