package me.wayne.allan;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.junit.Assert;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;

public class ToyRobotSimulatorStepDefinition {

    public static final String TEST_SCRIPTS_DIR = "src/test/resources/me/wayne/allan/test-scripts/";

    private File TRS_CLI_JAR = new File("../toy-robot-cli/target/trs-cli.jar");
    private File file;
    private String[] output;

    @Given("^a file \"(.*?)\"$")
    public void givenAFile(String filePath){
        file = new File(TEST_SCRIPTS_DIR,filePath);
        Assert.assertTrue(Files.exists(file.toPath()));
    }

    @When("^running the Toy Robot Simulator with the flag \"(-[a-zA-Z])\"")
    public void whenRunningTheToyRobotSimulatorWithTheFlag(String flag) throws IOException {
        System.out.println(TRS_CLI_JAR.getAbsolutePath());
        String commandStr = String.format("java -jar %s %s %s ", TRS_CLI_JAR.getAbsolutePath(), flag, file.getAbsolutePath());
        System.out.println(commandStr);
        CommandLine cmd = CommandLine.parse(
                commandStr
        );
        DefaultExecutor exec = new DefaultExecutor();
        OutputStream stdOut = new ByteArrayOutputStream();
        OutputStream stdErr = new ByteArrayOutputStream();
        exec.setStreamHandler(new PumpStreamHandler(stdOut,stdErr));
        int exitCode;
        try {
            exitCode = exec.execute(cmd);
        }catch (ExecuteException e){
            exitCode = e.getExitValue();
            System.err.println(e.getMessage());
        }
        output = stdOut.toString().split("\\n");
        Assert.assertEquals(exitCode, 0);
    }

    @Then("^the output on line \"(.*?)\" should contain the text \"(.*?)\"$")
    public void theOutputOnLineShouldContainTheText(int expectedLineNumber, String expectedText) throws Throwable {
        Assert.assertEquals(expectedText,output[expectedLineNumber-1]);
    }
}
